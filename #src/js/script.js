




function testWebP(callback) {

    let webP = new Image();
    webP.onload = webP.onerror = function () {
        callback(webP.height == 2);
    };
    webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}

testWebP(function (support) {

    if (support == true) {
        document.querySelector('body').classList.add('webp');
    }else{
        document.querySelector('body').classList.add('no-webp');
    }
});



let burgerButton = document.querySelector('.nav-toggle');

let headerMenu = document.querySelector('.header__wrapper-menu');

burgerButton.onclick= function (){

    if(burgerButton.classList.contains('active')){
        burgerButton.classList.remove('active');
        headerMenu.classList.remove('active');
    }
    else {
        burgerButton.classList.add('active');
        headerMenu.classList.add('active');
    }

}

